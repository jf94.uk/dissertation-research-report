\documentclass[
11pt,
twoside
]{report}


\usepackage[
a4paper,
vmargin=1in,
hmargin=1in
]{geometry}

\usepackage{setspace}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{multirow}


\usepackage{graphicx}
\graphicspath{{resources/images/}}


\usepackage[ backend=biber ]{biblatex}
\addbibresource{./resources/references.bib}

\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[C]{\ThesisTitle}
\fancyfoot{}
\fancyfoot[C]{\thepage}


\newcommand{\ThesisTitle}{Computational Imaging}
\newcommand{\TheAuthor}{Jose Fernandes}
\title{
{\ThesisTitle}\\
\hfill \\
{\large Heriot-Watt University}\\
\hfill \\
{\includegraphics[scale=2.5]{hw.jpg}} \\
\hfill \\
}
\author{\TheAuthor}
\date{22 November 2021}


\doublespacing
\begin{document}

\maketitle

\chapter*{Declaration}

I, \TheAuthor, confirm that this work submitted for assessment is my own and is expressed in my own words. Any uses made within it of the results of other authors in any form (e.g., ideas, equations, figures, text, tables, programs) are adequately acknowledged at any point of their use. A list of the references employed is included.

\hfill


Signed: \TheAuthor


Date: 22/11/2021

\chapter*{Abstract}

This Project describes creating a multi-stage algorithm to track and extract features from football matches' 2D video recording. The solution will track and map the players and ball's position relative to the court lines with minimal human assistance or monitoring. This algorithm will require implementing image processing and machine learning tools and techniques to allow visual recognition of intended elements. Although there are significant technical challenges, similar solutions were already attempted, which lay down the groundwork and references for guidance in this Project.


This solution aims to be an open platform for computer and sports scientists to collaborate and make invaluable contributions to the advancement of sports data science.




\tableofcontents


\chapter{Introduction}

In recent years, the importance of data analytics in sports allied with domain expertise has become ever more essential. A recent great example is the revival of one of the best football clubs globally, the Liverpool Football Club (LFC). The LFC was initially criticised for their analytical approach to running the football team, but now it is regarded as one of the most successful and profitable organisations in the sports industry. This organisation adopted innovative management strategies guided by advanced match analytics analysis to measure their performance and calculate the likelihood of their action's success (and profit) \cite{liverpool}.


Despite this encouraging story, \cite{liverpool}, and modern technology's availability, the advanced analytics data is only within reach of high-income clubs and organisations due to the costs involved \cite{opta}. The data providers rely primarily on human agents to label and annotate the actions and events in a match which drive the cost up \cite{opta}. These datasets are therefore not freely available for any small organisation, researcher or enthusiast that could contribute, for example, to various areas of Artificial Intelligence (AI), including Game Theory \cite{deepmind}.


The proposed solution is an open-source computer vision framework that extracts features from 2D video single-camera broadcasting, using image processing and machine learning tools, to output the position of players and ball sequentially. These principles can be applied to other sports, and the data produced will be vital to the sports data science community because it will encourage innovation in the sports field. This software will adopt an open-source license to encourage redistribution and collaboration between interested individuals and organisations, which will promote free access of football tracking data to anyone interested \cite{osd}.


This research report will cover the planning of a technical project. It will cover a discussion about similar research material and projects related, define the requirements and its evaluation strategy and describe the project management methodology for the Project. Any decision in this report may not be part of the final solution as requirements, or the project timeline can change based on unexpected events.


\chapter{Background}


This chapter will delve into current research, projects and articles related to computer vision for sports' data extraction. The interest in this field is recent. However, there is already a comprehensive set of information sources necessary to evaluate the feasibility of developing a potential solution. Most of these sources lay out the scientific and technical principles applied in developing their successfully tested and solutions being assessed. Therefore, it is vital to have a deeper analysis and reflection on these principles to determine the most appropriate approach for this Project. The review aims to gather the knowledge required to develop and evaluate this Project's solution successfully.


\section{Basketball video analysis}


The ``Open Source Sports Video Analysis using Machine Learning'' article \cite{stephan} is an excellent article that lays out a real-world application of the theory behind automated sports analytics video extraction. It serves as a complete description of the ``basketballVideoAnalysis'' open-source project \cite{stephan_code} that has the goal of tracking players, objects and actions for allowing the analysis of basketball match recordings. This article is a good starting point for this review because Stephan does a good job explaining complex theories in more straightforward terms and referencing other authoritative resources that must be considered to further expand on if anyone is interested in developing a similar solution. The Project used the ``Player tracking and analysis of basketball plays'' \cite{baskettrack} research as the blueprint for its planning and development. Stephan also suggests that this algorithm can be adapted for other collective sports. The article \cite{stephan} goes on to describe that the ``basketballVideoAnalysis'' project can track the match by executing a machine learning and image processing pipeline. This pipeline consists of basketball court detection, tracking humans inside the court, and mapping their movements to their position relative to the court.


Court detection is critical in this system because there is no reference needed to position objects (human bodies and ball) and filter out the noise (all remaining objects outside the court). The surface detection, in this article, is a pure image processing algorithm composed of naive OpenCV \cite{opencv} functionalities such as HSV conversion, Hue range filtering, Bitwise-AND masking, Canny edge detection and Hough Transformation. These functionalities' combination filters the input's noise out and increases the machine learning features' intensity.


The following step is to detect the people in the court, which is essential because it extracts the segmented image containing the person's to be used for further measures such as player identification, player tracking, homographic mapping, pose estimation and action recognition. The people detection is undertaken using robust common object detection models and training them against existing image curated datasets, for instance, the COCO image dataset \cite{cocodataset}.


The article points out how identifying players can be challenging by quoting an excerpt from ``Learning to Track and Identify Players from Broadcast Sports Videos'' \cite{learn_track_id} that describes the technical and logistical obstacles associated with this task.


The last step in Stephan's Project is to map the current camera view to a reference map to accurately determine where objects (including people) are in the field. In this operation, the OpenCV \cite{opencv} ``findHomography'' takes the original image points and the reference map coordinates to transform and map the original image onto the reference map.


Despite this article being a helpful starting point for research, it lacks academic research and implementation rigour. This is evident by the absence of any meaningful citation.


\section{Player tracking and analysis of basketball plays}

This project \cite{baskettrack} focuses on creating automated player detection and tracking system using the Matlab programming language. The basketball court detection in this Project is accomplished solely by using image processing techniques such as converting the colour format, eroding and dilating the image before applying the canny edge detector, and Hough transform. This process of court detecting appears to be very efficient as it is not a complex algorithm. This is highly effective for basketball, but it might not work as well in a football match. A football pitch is bigger than in basketball, and the camera is zoomed in to focus on a particular pitch zone. This fact, in football, forces a possible solution to find a way of augmenting the possible location the camera is focused on. The same pipeline, created by ``Player tracking and analysis of basketball plays'' research, should be used in our Project, but it needs to be complemented by machine learning and augmentation because basketball recordings have a significantly bigger percentage on the court \cite{baskettrack}.


The player detection is applied inside the court detection region by executing the OpenCV's \cite{opencv} HOG detector (histogram of oriented gradients) to create bounding boxes around players. This detector focuses on measuring the gradients' orientation on image's regions to detect patterns that recognise human bodies. This technique needs to be adjusted, during the testing stage, by adjusting the detection threshold adjusted to filter out false positives or just general noise. The approach is generally very effective unless two or more players are crossing in the camera view field. The authors decided to run the OpenCV's BGR detector inside the HOG detector bounding box since teams usually have different colours. This player detection algorithm is simple and accomplishes most of its purposes; however, it does not address players from the same team crossing the camera view. This event is very common in collective sports and should be a high requirement due to its occurrence frequency. An algorithm that uses human pose posture detection or other machine learning algorithms should effectively mitigate this important issue.


The team, following the implementation of the player detection algorithm, moved to work on player tracking. After some development, they identified four common tracking scenarios:
\begin{enumerate}
\item Player detected in consecutive frames: player was detected by HOG and BGR detectors;
\item Neighbourhood estimate: player was only detected by the BGR detector in the neighbourhood of the previous HOG detection;
\item Players added to frame;
\item Players removed from the frame.
\end{enumerate}
The first two scenarios are relatively easy to handle by applying the minimum distance correlator that calculates the most likely last position according to the current position. The last two cases are more complicated than the previous ones, and the Project did not mitigate them. Some of the reasons were the tracking of false positives (players on the bench just next to the court) and the camera jitter, which can affect the court view and the players' position on the camera view itself. The project \cite{baskettrack} was not successful in this step perhaps because it did not try to create an image recognition system that would train to learn the players' visual features (including the shirt number), as well as creating a machine-learning algorithm to augment the possible movement of players off the screen considering the last and first (when it reappears) position/speed.


Finally, the tracking data needs to be mapped to a model court by running a homography matrix. This matrix is generated through an affine transformation relative to the camera view towards the basketball court. This process is effective because the court and players' position is already known, and the matrix operation is very efficient in a numerical computing programming language like Matlab.



\section{Learning to track and identify players from broadcast sports videos}


The research analyses how to pre-process video and detect, identify and track players from a camera recording and map them to a referential map. It is a very extensive explanation that introduces video processing and player identification. Despite having many relevant requirements, the researchers decided not to have action recognition \cite{learn_track_id}.

Video pre-processing is composed of two steps: video segmentation and then ``play-by-play'' data processing. This pre-processing combination captures and aggregates relevant data used for data augmentation on the next pipeline layers. The video segmentation takes full recordings (sequence of different camera/video shots such as replays, ads, focus zoom), trains a complex model that uses Hidden Markov Model, Gaussian Mixture Model and the Viterbi algorithm to predict the segments and which type they are based on the colour distribution. The ``play-to-play'' data is a sequential, freely available online stream that tracks the most important match events. This type f data is very important in the player identification and could be very useful for action recognition if it was a requirement \cite{learn_track_id}.

The player tracking detects the players inside the court by running a Deformable Part Model, which can identify most players, but it has mediocre precision since it detects people in the audience and the referees, and it does not detect players obstructed in the camera angle. Before training a Linear Logistic Classifier, the false positives (audience and referee) are removed by applying team colour (since all teammates have the same colour but are different from the opposition or referee). The player tracking follows the bounding boxes' centre points from the player detections and tries to predict the following position based on the Euclidean distances of previous positions by executing a linear-Gaussian transition model. The precision of this tracking technique has a very high precision since it only tracks single points (simple and minimal algorithm) and drops sparse tracking or view borders signals \cite{learn_track_id}.
The player identification is an exciting and unique challenge set by the researchers. It poses serious questions about how its efficiency due to:

\begin{itemize}
\item same team players have the same kits;
\item recording with resolution too low for facial recognition;
\item some players might have similar body characteristics, hairstyles, or boots;
\item shirt numbers and names are deformed due to constant posture changes \cite{learn_track_id};
\end{itemize}


The solution for player identification focuses on learning players' visual features/characteristics, dismissing trying to identify players' faces \cite{learn_track_id}. The input to identify these features are:

\begin{itemize}
\item Maximal extremal regions (MSRE);
\item Scale-invariant feature transform (SIFT);
\item RGB colour histograms;
\end{itemize}


These image processing features and the bounding box image segment are the input for a complex algorithm that continuously learns each player's visual characteristics and identifies them \cite{learn_track_id}.
The homography transformation is calculated by representing the referential basketball court as a set of points. The Canny Edge detector processes the image with a certain threshold to remove noise and drop nonlinear or well-rounded lines until only the court shape is visible. Following this process constructs the homography matrix by incrementally detecting the corresponding points (nearest points) until it converges in at most five iterations, at most \cite{learn_track_id}.

\section{Multi-Person 3D Pose Estimation and Tracking in Sports}






This Project aims to process multiview recordings, detect and correct players' 2D pose estimations, associate the 2D poses into a 3D pose and track the 3D poses. Pose estimation is very important because it has the potential to allow for action recognition. The researchers consider this Project as the pioneering in full 3D pose estimation tracking for sports. The Project used advanced research from previous work on 3D pose estimation and multiview video recording to develop a very optimised process to capture this kind of data for Football \cite{cvpr_2019}. The algorithm follows these general steps:

\begin{itemize}
\item 2D pose estimation
\item 2D pose error correction
\item 2D pose multiview association to 3D pose
\item 3D skeleton tracking
\end{itemize}

The Project uses a CNN-based model to train in the ``Surreal'' synthetic dataset \cite{surreal} and subsequently estimate the 2D pose of players.
The initial pose estimation is prone to errors due to low-quality image, view cut, and multi-person fusion, so the pose error correction is vital to correct these common errors. The researchers decided on a per-frame pose correction solution rather than a temporal filtering and tracking algorithm because its performance is significantly better \cite{cvpr_2019}.
The next step is to associate the two 2D pose estimations from a single frame by running a greedy algorithm that measures the pair of corresponding points. Once this correspondence is asserted, then the 3D joint locations can be calculated. Once the 3D joint location is calculated, then it can be constructed and mapped to the 3D court model \cite{cvpr_2019}.
The final step is to sequentially track the resultant 3D pose models around the pitch what is very impressive because it is a full 3D model of a football match \cite{cvpr_2019}.






\section{Soccer: Who Has The Ball? Generating Visual Analytics and Player Statistics}


This Project is a solution that generates visual analytics and player statistics from football recordings. This is an important paper because it aims to recognise actions using deep neural networks from single frames in football matches. The paper also describes the architecture of the different deep neural networks used for, as mentioned before, for the action recognition but also the player location detection \cite{cvpr_2018}.




The first part of this algorithm is to locate, track the player and detect the team. The location is done by running the YOLO algorithm, which predicts the bounding boxes containing every player on the camera view. These bounding boxes are then tracked during the consecutive by another model also using the YOLO algorithm. This tracking phase is then finished by running a Histogram Matching algorithm inside the bounding box to identify the player's team. The Histogram Matching algorithm was chosen because it is fast at detecting different colours (teams' kits are always different), and the task is relatively simple since the player is inside a small region \cite{cvpr_2018}.






The last step is to identify if the player is "in control of the ball" or not and then augment data from this information. The researchers created an image dataset with images (segments of images, zoomed into the player) of individual football players and then classified the images with either "player with the ball" or "player without the ball" classes. This part of the algorithm then took the synchronised sequence of images and predicted data to keep track of the player holding the ball. This information is relevant because it is possible to deduce with spatio-temporal precision the possession of the ball by a team since it is the sequence of players "having the ball". The spatio-temporal event between players of the same team "having the ball" is a pass (where the start and end location is also known) otherwise is an opposition "ball recovery". Generating an automatic model of possession is precious because it is the most frequent activity the football teams engages in, and it is the most relevant way football domain experts take into consideration when analysing football \cite{cvpr_2018}.




\section{Conclusion}




This background research was vital to understanding the foundations of the current solutions in the field. Based on the current acquired information, this Project's goal is possibly accomplished by decomposing the problem into a pipeline composed by:

\begin{itemize}
\item Court detection;
\item Object detection;
\item Team identification;
\item Object tracking;
\item Player identification;
\item Homographic mapping of objects.
\end{itemize}


The important idea to remember is that there is already robust technologies and methodologies necessary for this solution. This knowledge gained should provide a solid foundation to develop a proper implementation of a similar solution.


\chapter{Requirements}



This section covers the requirements analysis and evaluation strategy. The requirements define the Project's scope and constraints which are critical for guiding all the Project's activities. The evaluation will define how to test and measure the effectiveness and efficiency of the final solution.


\section{Requirements analysis}


The requirements analysis will focus on defining the many requirements needed to be respected for this project's success. The requirements are intended to be clear and testable to be referenced throughout the Project's lifecycle. This analysis will cover the functional and non-functional requirements.


The requirements will be prioritised using the ``Moscow'' methodology that sets many different levels of importance for the requirements. The priorities levels are:

\begin{enumerate}
\item ``Must have'': critical level requirements that the Project needs to comply with to achieve any level of success;
\item ``Should have'': important requirements that contribute to a solid project;
\item ``Could have'': relevant requirements to achieve a high-quality standard;
\item ``Want to have'': suggestive requirements that will only be attempted if there is enough time;
\end{enumerate}


\newpage


\subsection{Functional}

\hfill


These requirements define the solution's functionalities. This is important because it establishes the priority of "what" the solution must do. In this Project, the functionalities will focus on computer vision, machine learning and cloud computing functionalities.


\hfill


\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | p{3.5in} |}
\cline{1-3}
Priority & Req. ID & Description \\
\hline

\multirow{5}{*}{Must have} & F1 & Detect Court \\
\cline{2-3}
& F2 & Detect Objects \\
\cline{2-3}
& F3 & Identify players \\
\cline{2-3}
& F4 & Track Objects \\
\cline{2-3}
& F5 & Map objects to referential court \\


\cline{1-3}


\multirow{6}{*}{Should have} & F6 & Detect view segmentation \\
\cline{2-3}
& F7 & Correct and edit output data \\
\cline{2-3}
& F8 & Synchronise positional and event data \\
\cline{2-3}
& F9 & Process match line up sheet \\
\cline{2-3}
& F10 & Smooth input and output data \\
\cline{2-3}
& F11 & Recognise actions \\


\cline{1-3}

\multirow{7}{*}{Could have} & F12 & Create extra/synthetic points for homography recognition \\
\cline{2-3}
& F13 & Detect match period \\
\cline{2-3}
& F14 & Predict off-screen players' position \\
\cline{2-3}
& F15 & Recognise referees, line refereeing actions \\
\cline{2-3}
& F16 & Detect set pieces \\
\cline{2-3}
& F17 & Detect goals \\
\cline{2-3}
& F18 & Detect substitutions \\
\cline{1-3}


\multirow{5}{*}{Want to have} & F19 & Estimate ball trajectory \\
\cline{2-3}
& F20 & Estimate body language from posture \\
\cline{2-3}
& F21 & Create language to process data output \\
\cline{2-3}
& F22 & Recognise commentary and match audio \\
\cline{2-3}
& F23 & Process broadcasting graphics \\


\cline{1-3}
\end{tabular}
\caption{Functional requirements table}
\label{tab:func_req}
\end{center}
\end{table}



\newpage

\subsection{Non Functional}

\hfill


These requirements specify the Project's behaviour which is crucial because it sets the constraints for the success of this endeavour. There is a wide range of constraints laid out here, ranging from regulatory demands to technical issues.


\hfill

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | p{3.5in} |}
\cline{1-3}
Priority & Req. ID & Description \\
\hline

\multirow{9}{*}{Must have} & N1 & Work with 2d broadcasting \\
\cline{2-3}
& N2 & Robust \\
\cline{2-3}
& N3 & Able to process at least a full game \\
\cline{2-3}
& N4 & Able to train and run on cloud \\
\cline{2-3}
& N5 & Open-source license \\
\cline{2-3}
& N6 & Use open-source libraries \\
\cline{2-3}
& N7 & Include explicit reference to people's consent for their tracking \\
\cline{2-3}
& N8 & Train, test and run under the free account services limits \\

\cline{1-3}


\multirow{8}{*}{Should have} & N9 & Handle different stadium architecture \\
\cline{2-3}
& N10 & Handle different camera angle and filming characteristics \\
\cline{2-3}
& N11 & Handle different meteorological conditions \\
\cline{2-3}
& N12 & Trainable against other available sample data \\
\cline{2-3}
& N13 & Can be containerised \\
\cline{2-3}
& N14 & Predictive performance \\
\cline{2-3}
& N15 & Consistent data format \\
\cline{2-3}
& N16 & Allow analyst to edit/correct data \\


\cline{1-3}


\multirow{4}{*}{Could have} & N17 & Modular architecture \\
\cline{2-3}
& N18 & Provide free and re-distributable testing/training datasets \\
\cline{2-3}
& N19 & Process multiple camera environment \\
\cline{2-3}
& N20 & User documentation \\


\cline{1-3}


\multirow{4}{*}{Want to have} & N21 & Real time processing \\
\cline{2-3}
& N22 & Public access dataset repo \\
\cline{2-3}
& N23 & Webpage \\
\cline{2-3}
& N24 & Modular to allow adaptation for other sports \\



\cline{1-3}
\end{tabular}
\caption{Non-functional requirements table}
\label{tab:non_func_req}
\end{center}
\end{table}




\newpage


\section{Evaluation Strategy}




The success of this Project depends on having an appropriate evaluation strategy. This strategy aims to improve the machine and computer vision system's performance and mitigate its natural tendency to develop a bias. The evaluation approach must be flexible since this problem is inherently nuanced and convoluted.






\subsection{Machine learning evaluation techniques}




The Project will make heavy use of machine learning libraries. These libraries are very mature software that includes the end-to-end data mining and machine learning stack. The project evaluation will be based on the built-in evaluation tools that can extract performance metrics from running these algorithms. Some of these metrics \cite{mlevaluation} are, for example:
\begin{itemize}
\item Confusion matrix;
\item Specificity and Sensitivity;
\item Precision and Recall;
\item Accuracy;
\item Error rate;
\end{itemize}


These metrics will be applied to measure how the algorithm is performing from different technical points because of the complexity of the data.




\subsection{Testing datasets}




As mentioned previously, the existing companies in this industry provide sample match datasets containing a limited number of football matches. The data is usually manually annotated and verified by highly trained teams in these organisations to ensure the data is precise.
These datasets, along with their corresponding datasets, can be very useful for testing the solution's accuracy. This step will verify how well this system would work in a real scenario because the input video recording data is completely unknown, and the testing match event dataset accurately represents what is happening in the recording.




\subsection{Manual verification}




Manual verification is normally not used in many evaluation strategies, but it can be the last resource for certain circumstances. The manual approach can be useful despite the machine learning evaluation techniques due to the nuanced and complex nature of computer vision problems that require human manual supervision. This supervision will only be applied on particular scenarios where the model may not be effective yet since it is not scalable to verify the entire dataset.


\chapter{Project Management}


This section will examine serious aspects of this Project that require deeper analysis and reflection to ensure that the most relevant issues were already prevented or were identified (and their mitigation was planned) at least. These aspects are tied to the Project's organisation and logistics and potential future technical and other general issues.



\section{Project Plan}

\hfill


The project timeline is dedicated to two main activities: Documentation and Project.

\hfill

\includegraphics[scale=0.245]{project.jpg}


The Documentation is composed of three sequential tasks:
\begin{itemize}
\item Research report: the activity that outputs the "Deliverable 1" by the first deadline (23/11/2021);
\item Dissertation: the activity that outputs the "Dissertation submission" by the second deadline (24/04/2022);
\item Presentation: the activity that outputs the "Online Session" by the second deadline (13/05/2022);
\end{itemize}

\newpage

The Project is composed of two concurrent tasks:
\begin{itemize}
\item Development: all activity related to the framework building;
\item Evaluation: all activity related with the framework evaluating and testing;
\end{itemize}
These Project's tasks are concurrent because they output the "Solution's first version".


\section{Risks Analysis}

This section covers the identification of risks and their mitigation planning. The risks are directly tied with the project requirements to ensure the predicted risks are reasonable and handle the Project's expectations.


\newpage


\subsection{Potential Risks}

\hfill

The table below, identifies and enumerates the risks, links them to the requirements and categorises them.

\hfill


\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | p{2in} |}

\cline{1-4}
Risk ID & Category & Requirements & Description \\

\hline
R1 & Project Management & F[1-5], N[1-8] & project's critical requirements are not met \\
\hline
R2 & System & N5, N6, N13, N17 & different components do not integrate \\
\hline
R3 & Computer vision & N1, N10 & does not handle different camera angles \\
\hline
R4 & Project Management & N12 & testing sample data sets are not available for download \\
\hline
R5 & Computer vision & F1, N10, N11 & does not handle different meteorological conditions \\
\hline
R6 & System & N5, N6 & open source libraries are not good enough for implementation \\
\hline
R7 & System & N2, N14 & no consistent performance \\
\hline
R8 & System & N14 & system does not process single match video data on regular personal pc \\
\hline
R9 & Project Management & F[1-5], N[1-8] & Completion time, for stable version, was underestimated \\
\hline
R10 & Project Management & N7 & project infringes GDPR \\
\hline
R11 & System & F[1-5] & System is inefficient \\
\hline
R12 & Project Management & Every req. & requirements will change \\
\hline
R13 & Machine Learning & N12 & is not trainable against open-datasets \\
\hline
R14 & System & N2, N4, N14, N21 & System is too unstable \\
\hline
R15 & Project Management & N8 & project's free cloud services limits are exceeded\\
\hline
R16 & System & N4 & does not run on cloud \\
\hline
R17 & Project Management & N5, N6 & libraries used infringe various licenses \\
\hline
R18 & Machine Learning & F1 & does not detect court \\
\hline
R19 & Machine Learning & F2 & does not detect objects \\
\hline
R20 & Machine Learning & N15 & does not synchronise event and positional data \\
\hline



\end{tabular}
\caption{Potential risks table}
\label{tab:pot_risks}
\end{center}
\end{table}



\newpage
\subsection{Risks Planning}



\hfill

The table below, categorises the probability of occurrence and the impact of the previously identified risks and plans to mitigate their effects.

\hfill


\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | p{3in} |}

\hline
Risk ID & Probability & Impact & Action \\

\hline
R1 & Rare & Severe & Focus on prioritising tasks related to critical requirements \\
\hline
R2 & Unlikely & Moderate & Use common and well tested combinations of components \\
\hline
R3 & Unlikely & Major & Create diverse and balanced image dataset \\
\hline
R4 & Possible & Minor & Manually annotate a very small set of matches for testing only \\
\hline
R5 & Possible & Moderate & Create diverse and balanced image set \\
\hline
R6 & Rare & Severe & Use more computer vision functionalities and create own machine learning algorithm \\
\hline
R7 & Possible & Major & Follow researched projects implementations \\
\hline
R8 & Almost Certain & Trivial & Proceed to develop and run program on a cloud platform \\
\hline
R9 & Possible & Moderate & Create an effective project management strategy \\
\hline
R10 & Rare & Severe & Create a mechanism to manage tracked people's data sharing consent \\
\hline
R11 & Possible & Moderate & Follow researched projects implementations \\
\hline
R12 & Almost certain & Minor & Create a flexible and adaptable project plan \\
\hline
R13 & Unlikely & Minor & Prepare a specific algorithm for testing \\
\hline
R14 & Unlikely & Severe & Only use libraries' stable versions \\
\hline
R15 & Rare & Moderate & Continuously track resources use by solution \\
\hline
R16 & Rare & Major & Develop the solution on the cloud, before deployment \\
\hline
R17 & Unlikely & Severe & Do research on the compatibility of various licenses \\
\hline
R18 & Unlikely & Severe & Assign this as a very urgent requirement and follow existing implementations \\
\hline
R19 & Unlikely & Severe & Assign this as a very urgent requirement and follow existing implementations \\
\hline
R20 & Possible & Moderate & Assign this as an critical requirement and follow similar implementations \\



\hline
\end{tabular}
\caption{Risks planning table}
\label{tab:risks_plan}
\end{center}
\end{table}


\newpage






\section{Potential issues}


The successful development of any endeavour depends on a deeper reflection about the potential risks it might cause. It is important to identify the possible pitfalls so that they may be prevented or mitigated at the least. This segment will address this issue to ensure this Project causes a positive impact on greater society.



\subsection{Professional}



It is imperative to uphold the highest professional standards to ensure the sustainability and longevity of any serious project. A professional must regard the public interest, must only undertake activities within the profession and has a duty to the relevant authority and the profession itself \cite{bcs}.




The Project has a deep interest in contributing to the public by providing free, open-sourced, and redistributable software. This type of license is known and recognised for protecting public health, privacy, security as well as protecting the environment while respecting other competitors and researchers' (third parties) licensing and intellectual property rights \cite{stallman} \cite{bcs}.




The computer science field will bind the initial Project's scope because it is the researcher and engineer's expertise. The Project expects future contributions from other professionals from the same and different fields. The professionals taking part in this Project must uphold the most up to date legislation and industry standards relevant to their activities. The professionals must also be aware of working in multi-disciplinary by valuing and respecting the contribution of others \cite{bcs}.




The Heriot-Watt University is the relevant authority for this Project as it supervises and defines the higher-level requirements and constraints required for any project. The Project must also consider the university's interests to not misrepresent it or create a conflict of interest against it since it is the organisation granting the opportunity to research about and develop this solution.




Ultimately the work done in this Project should reflect the profession's integrity and reputation, and the members need to seek how to continuously improve their professional standards to ensure the solution meets the highest quality and ethical standards.

\subsection{Legal}


Data protection is critical in software that tracks human beings. For this reason, the Project must provide reference to people's consent; otherwise, it will not run without the explicit consent of everyone involved. This consent informs why the data is being collected, how it is processed, and how long it will be stored. The measure follows the current GDPR \cite{gdpr}, still active in the UK, to control the handling of personal data.


The program licensing could lead to serious legal issues. This Project aims to not infringe on any existing solution's copyright or intellectual property rights. These are legal rights that must protect the works of individuals and organisations \cite{iproperty} \cite{copyright}.

\subsection{Ethical}


The emphasis on adopting an open-source license is a pragmatic approach to ensure that users are aware of how the program does its computing, give freedom to its users and encourage collaboration across the industry \cite{stallman}. This is very important because it is the reason this Project was proposed. There are already a few companies operating this kind of software, but their proprietary licensed software excludes anyone interested from potentially contributing to the development of sports data science. These companies offer free data for research or competitions but only under very restrictive conditions, primarily for commercial reasons \cite{opta}.


Therefore, the ideal and ethical solution should be accessible to anyone enthusiastic about sports data science interested in contributing to the sports and data sciences communities.


\subsection{Social}


This venture aims to provide equality of opportunity to everyone contributing to the Project or monitoring the program. People should not be held back for any prejudice or harm that may be intentionally or unintentionally targeted at them. The social responsibility of this Project is to be mindful of unwanted consequences of the running of this software.


As a machine learning project, there is the issue of developing software discriminating against people regarding immutable characteristics. It is imperative to avoid this cruelty by using the proper data science's tools and techniques to evaluate the software against any inadmissible discriminatory bias.


\printbibliography

\end{document}
