\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{6}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Background}{8}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Basketball video analysis}{8}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Player tracking and analysis of basketball plays}{9}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Learning to track and identify players from broadcast sports videos}{11}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Multi-Person 3D Pose Estimation and Tracking in Sports}{12}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Soccer: Who Has The Ball? Generating Visual Analytics and Player Statistics}{13}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Conclusion}{14}{section.2.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Requirements}{16}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Requirements analysis}{16}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Functional}{17}{subsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Non Functional}{18}{subsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Evaluation Strategy}{19}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Machine learning evaluation techniques}{19}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Testing datasets}{19}{subsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Manual verification}{20}{subsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Project Management}{21}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Project Plan}{21}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Risks Analysis}{22}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Potential Risks}{23}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Risks Planning}{24}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Potential issues}{25}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Professional}{25}{subsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Legal}{26}{subsection.4.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Ethical}{26}{subsection.4.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Social}{26}{subsection.4.3.4}%
