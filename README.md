# Technical Project

# Todos
+ example


# Description
focus on implementing some software and evaluating it or developing a formal framework and proving its correctness.

## Research tasks:
+ Conduct a literature review of existing related work.
+ Identify system requirements.
+ Outline the initial system design and technology choices.
+ Define the evaluation strategy.




# Marking
+ Background literature: thoroughness of review, appropriateness of references, organisation, presentation and use of references, conclusions reached.
+ Requirements section: amount of detail, organisation, thoroughness.
+ Schedule of work: detail, realism.
+ General presentation, organisation, length.




# Structure

## Front Matter
### Title page
> including the following details:
> + Title of your project.
> + Your full name.
> + Your supervisors’ names.
> + The caption “Deliverable 1: Final Year Dissertation”.
> + The degree programme for which you are studying.

### Declaration
> confirming that the dissertation is your own work (see Appendix C.1 ).

### Abstract
> a short description of the project and the main work to be carried out – probably between one and two hundred words.

### Table of Contents
> giving the main chapter and section titles and the pages on which they start.


## Main Body
### Introduction
> summarising the context of the dissertation project, stating the aim and objectives of the project, identifying the problems to be solved to achieve the objectives, and sketching the organisation of the dissertation.
> give an example



### Background
> discussing related work found in the technical literature and its relevance to your project.








guidelines
• How relevant is the literature that is covered?
• Is there missing material?
• Is it well structured?
• Are good quality sources used and properly cited?
• How strong are the comparative and critical aspects?
• Is the literature review of an appropriate length?






### Requirements Analysis
> This is required for technical projects and should be linked back to the project aim and objectives. It should provide a detailed use case scenario and suitable use case descriptions, user requirements, and MoSCoW analysis of the requirements.


Guidelines
- Are the requirements and/or hypothesis/research questions clearly
expressed, testable, and achievable?
- Are there details for any strategy for testing and evaluation
### Risk Analysis
potential risks
risks planning




### Design (optional)
> an initial design of software
Project decisions

### Evaluation Strategy
> Details of the evaluation and analysis to be conducted.
### Project Management
> This section should include:
> + A well-researched consideration of any Professional, Legal, Ethical, and Social Issues pertinent to the project. (e.g. codes of conduct (BCS), codes of practice, standards, computer law, ethical decision making, intellectual property, social aspects, copyright, data protection, and so on).

IMPORTANT professional, legal, ethical, and social issues

Budget Analysis



## Back Matter
### References
> listing complete details of all the documents cited in the text.
### Appendices
> to include additional material, consult with your supervisor.


# 4 P's

describe the problem

+ position: Football analytics are making the industry more efficient
+ problem: data providers charge high fees fort their data feeds
+ possibilities: use available image processing, data mining and machine learning tools, free as in freedom software available for everyone
+ proposal: create a free as in freedom computer vision solution to generate


